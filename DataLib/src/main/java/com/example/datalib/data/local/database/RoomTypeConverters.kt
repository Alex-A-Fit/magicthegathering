package com.example.datalib.data.local.database

import androidx.room.TypeConverter

class RoomTypeConverters {
    @TypeConverter
    fun fromStringToList(value: String?): List<String>? {
        return value?.split(",")?.toList()
    }

    @TypeConverter
    fun ListToString(value: List<String>?): String? {
        return value?.joinToString(",")
    }

}