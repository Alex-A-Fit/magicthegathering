package com.example.datalib.data.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.datalib.data.local.MtgDao
import com.example.datalib.domain.models.extendedmodels.*
import com.example.datalib.domain.models.random_card_model.RandomCardModel

@Database(entities = [RandomCardModel::class], exportSchema = false, version = 1)
@TypeConverters(RoomTypeConverters::class)
abstract class MtgDb: RoomDatabase() {
    abstract fun MtgDao() : MtgDao
}