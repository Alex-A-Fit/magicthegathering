package com.example.datalib.data.repo

import com.example.datalib.data.local.database.MtgDb
import com.example.datalib.di.RepoModule
import com.example.datalib.domain.datainterfaces.LocalDataSource
import com.example.datalib.domain.datainterfaces.RemoteDataSource
import com.example.datalib.domain.models.random_card_model.RandomCardModel
import com.example.datalib.domain.models.random_card_model.RandomCardModelApi
import com.example.datalib.util.Resource
import javax.inject.Inject

class Repository @Inject constructor(
    private val local: LocalDataSource,
    private val remote: RemoteDataSource,
    private val db : MtgDb
    ) {
    private var mtgDao = RepoModule.provideDao(db)

    suspend fun getRandomCard(): Resource<RandomCardModel> {
        val randomCard = remote.getRandomCard()
        var cardInDb = mtgDao.getRandomCard()?: ""
        if (randomCard.isSuccessful){
            val body = randomCard.body()
            val newCardToDb= RandomCardModel(
                body?.id!!,
                body.image_uris.art_crop,
                body.image_uris.normal,
                body.image_uris.small,
                body.mana_cost,
                body.name,
                body.nonfoil,
                body.rarity,
                body.released_at,
                body.reprint,
                body.reserved,
                body.prices.usd,
                body.prices.usd_etched,
                body.prices.usd_foil
            )
            if(cardInDb == ""){
                local.addRandomCardToDb(newCardToDb)
            }else{
                cardInDb = mtgDao.getRandomCard()
                local.deleteRandomCardFromDb(cardInDb)
                local.addRandomCardToDb(newCardToDb)
            }
        cardInDb = mtgDao.getRandomCard()
            return Resource.Success(cardInDb)
        }
        else{
            return Resource.Error("Response Unsuccessful")
        }
    }
}