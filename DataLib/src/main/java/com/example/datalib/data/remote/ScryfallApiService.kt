package com.example.datalib.data.remote

import com.example.datalib.domain.models.random_card_model.RandomCardModelApi
import retrofit2.Response
import retrofit2.http.GET

interface ScryfallApiService {

    @GET("cards/random")
    suspend fun getRandomCard(): Response<RandomCardModelApi>

}