package com.example.datalib.data.remote

import android.util.Log
import com.example.datalib.domain.datainterfaces.RemoteDataSource
import com.example.datalib.domain.models.random_card_model.RandomCardModel
import com.example.datalib.domain.models.random_card_model.RandomCardModelApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(private val scryfallApi: ScryfallApiService): RemoteDataSource {

    override suspend fun getRandomCard(): Response<RandomCardModelApi>  = withContext(Dispatchers.IO){
        return@withContext scryfallApi.getRandomCard()
    }
}