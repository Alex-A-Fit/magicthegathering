package com.example.datalib.data.local

import androidx.room.*
import com.example.datalib.domain.models.extendedmodels.ImageUris
import com.example.datalib.domain.models.random_card_model.RandomCardModel

@Dao
interface MtgDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addRandomCard (card: RandomCardModel)

    @Query("SELECT * FROM RandomCardTable WHERE id = :id")
    suspend fun getCardByID (id: String) : RandomCardModel

    @Query("SELECT * FROM RandomCardTable")
    suspend fun getRandomCard() : RandomCardModel

    @Delete
    suspend fun deleteRandomCard(card: RandomCardModel)

}