package com.example.datalib.data.local

import com.example.datalib.data.local.database.MtgDb
import com.example.datalib.domain.datainterfaces.LocalDataSource
import com.example.datalib.domain.models.random_card_model.RandomCardModel
import com.example.datalib.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class LocalDataSourceImpl @Inject constructor(database: MtgDb): LocalDataSource {

    private val mtgDao = database.MtgDao()

    override suspend fun addRandomCardToDb(card: RandomCardModel) = withContext(Dispatchers.IO){
        mtgDao.addRandomCard(card)
    }

    override suspend fun getRandomCardFromDb(): Resource<RandomCardModel> = withContext(Dispatchers.IO){
        return@withContext Resource.Success(mtgDao.getRandomCard())
    }

    override suspend fun deleteRandomCardFromDb(card: RandomCardModel) = withContext(Dispatchers.IO){
        mtgDao.deleteRandomCard(card)
    }

    override suspend fun getCardByID(id: String): Resource<RandomCardModel> = withContext(Dispatchers.IO){
        return@withContext Resource.Success(mtgDao.getCardByID(id))
    }

}