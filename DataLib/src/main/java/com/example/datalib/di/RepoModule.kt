package com.example.datalib.di

import android.content.Context
import androidx.room.Room
import com.example.datalib.data.local.MtgDao
import com.example.datalib.data.local.LocalDataSourceImpl
import com.example.datalib.data.local.database.MtgDb
import com.example.datalib.data.remote.RemoteDataSourceImpl
import com.example.datalib.data.remote.ScryfallApiService
import com.example.datalib.domain.datainterfaces.LocalDataSource
import com.example.datalib.domain.datainterfaces.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepoModule {

    @Provides
    @Singleton
    fun provideDao(db: MtgDb): MtgDao = db.MtgDao()

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context): MtgDb =
        Room.databaseBuilder(context, MtgDb::class.java,"MTG_DB")
            .fallbackToDestructiveMigration().build()

    @Provides
    @Singleton
    fun provideLocalDataSource(database : MtgDb): LocalDataSource = LocalDataSourceImpl(database)


    @Provides
    @Singleton
    fun provideApiService(): ScryfallApiService {
        return Retrofit
            .Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.scryfall.com/")
            .build()
            .create(ScryfallApiService::class.java)
    }

    @Provides
    @Singleton
    fun provideRemoteDataSource(apiService: ScryfallApiService): RemoteDataSource = RemoteDataSourceImpl(apiService)
}