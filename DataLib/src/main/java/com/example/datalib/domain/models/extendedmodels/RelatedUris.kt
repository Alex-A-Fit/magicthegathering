package com.example.datalib.domain.models.extendedmodels

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "RelatedUris")
@Parcelize
data class RelatedUris(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val edhrec: String,
    val gatherer: String,
    val tcgplayer_infinite_articles: String,
    val tcgplayer_infinite_decks: String
): Parcelable