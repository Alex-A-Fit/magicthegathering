package com.example.datalib.domain.models.extendedmodels

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
data class Prices(
    val eur: String,
    val eur_foil: String,
    val tix: String,
    val usd: String,
    val usd_etched: String,
    val usd_foil: String
): Parcelable