package com.example.datalib.domain.datainterfaces

import com.example.datalib.domain.models.random_card_model.RandomCardModelApi
import retrofit2.Response

interface RemoteDataSource {
    suspend fun getRandomCard(): Response<RandomCardModelApi>
}