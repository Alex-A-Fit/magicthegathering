package com.example.datalib.domain.models.random_card_model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.datalib.domain.models.extendedmodels.*
import kotlinx.parcelize.Parcelize

@Entity(tableName = "RandomCardTable")
@Parcelize
data class RandomCardModel(
    @PrimaryKey(autoGenerate = false)
    val id: String = "",
//    val image_uris: ImageUris,
    val art_crop: String?,
    val normal: String?,
    val small: String?,
    //END
    val mana_cost: String,
    val name: String,
    val nonfoil: Boolean,
    val rarity: String,
    val released_at: String,
    val reprint: Boolean,
    val reserved: Boolean,
    //     Prices,
    val usd: String?,
    val usd_etched: String?,
    val usd_foil: String?

): Parcelable