package com.example.datalib.domain.models.extendedmodels

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Parcelize
data class Legalities(
    val id: String,
    val alchemy: String,
    val brawl: String,
    val commander: String,
    val duel: String,
    val explorer: String,
    val future: String,
    val gladiator: String,
    val historic: String,
    val historicbrawl: String,
    val legacy: String,
    val modern: String,
    val oldschool: String,
    val pauper: String,
    val paupercommander: String,
    val penny: String,
    val pioneer: String,
    val premodern: String,
    val standard: String,
    val vintage: String
): Parcelable