package com.example.datalib.domain.models.extendedmodels

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize


@Parcelize
data class ImageUris(
    val id: String,
    val art_crop: String,
    val border_crop: String,
    val large: String,
    val normal: String,
    val png: String,
    val small: String
):Parcelable