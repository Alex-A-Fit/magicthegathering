package com.example.datalib.domain.models.extendedmodels

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "PurchaseUris")
@Parcelize
data class PurchaseUris(
    @PrimaryKey(autoGenerate = false)
    val id: String,
    val cardhoarder: String,
    val cardmarket: String,
    val tcgplayer: String
): Parcelable