package com.example.datalib.domain.datainterfaces

import com.example.datalib.domain.models.random_card_model.RandomCardModel
import com.example.datalib.util.Resource

interface LocalDataSource {
    suspend fun addRandomCardToDb(card: RandomCardModel)

    suspend fun getRandomCardFromDb(): Resource<RandomCardModel>

    suspend fun deleteRandomCardFromDb(card: RandomCardModel)

    suspend fun getCardByID (id: String): Resource<RandomCardModel>
}