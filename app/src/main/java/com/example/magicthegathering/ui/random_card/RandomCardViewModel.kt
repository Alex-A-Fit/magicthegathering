package com.example.magicthegathering.ui.random_card

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.datalib.data.repo.Repository
import com.example.datalib.domain.models.random_card_model.RandomCardModel
import com.example.datalib.util.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class RandomCardViewModel @Inject constructor(private val repo: Repository) : ViewModel() {

    private var _randomCard: MutableLiveData<Resource<RandomCardModel>> = MutableLiveData(Resource.Loading())
    val randomCard: LiveData<Resource<RandomCardModel>> get() = _randomCard

    fun getRandomCard() = viewModelScope.launch{
        _randomCard.value = repo.getRandomCard()
    }
}