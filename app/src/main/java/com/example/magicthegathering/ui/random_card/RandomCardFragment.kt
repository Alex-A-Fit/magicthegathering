package com.example.magicthegathering.ui.random_card

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.datalib.util.Resource
import com.example.magicthegathering.R
import com.example.magicthegathering.databinding.FragmentRandomCardBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RandomCardFragment : Fragment() {

    private var _binding: FragmentRandomCardBinding? = null
    private val binding: FragmentRandomCardBinding get() = _binding!!
    private val viewModel by viewModels<RandomCardViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentRandomCardBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.insertTopAppBar.topAppBar.title = "Random Card"
        initListener()
        initObservers()
    }

    private fun initListener() {
        val action = RandomCardFragmentDirections.actionNavigationRandomCardToCardDetailsFragment()
        fun goToDetails() = findNavController().navigate(action)
        binding.btnGenerateRandomCard.setOnClickListener {
            viewModel.getRandomCard()
        }
    }

    private fun initObservers(){
        viewModel.randomCard.observe(viewLifecycleOwner){ randomCardState ->
            when(randomCardState){
                is Resource.Error -> {}
                is Resource.Loading -> {}
                is Resource.Success -> {
                    binding.tvRandomCardTitle.text = randomCardState.data.name
                    binding.ivRandomCardImg.loadImage(randomCardState.data.normal!!)
                }
            }

        }
    }

    override fun onDestroyView() {
    super.onDestroyView()
    _binding = null
}
    private fun ImageView.loadImage(url: String) {
        Glide.with(context).load(url).into(this)
    }
}